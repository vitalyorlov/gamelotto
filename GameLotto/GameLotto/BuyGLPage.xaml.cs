﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace GameLotto
{
    public partial class BuyGLPage : ContentPage
    {
        public BuyGLPage()
        {
            InitializeComponent();

            this.Icon = "menu.png";
            NavigationPage.SetTitleIcon(this, this.Icon);
            this.BackgroundColor = Color.White;

            TableView tableView = new TableView { RowHeight = 100, Intent = TableIntent.Form };
            
            TableSection tableSection = new TableSection() ;
            
            tableView.Root.Add(tableSection);
            
            tableSection.Add(new BuyGlCell("5000 GL", "За 15$", buy5000Gl) { Height = 300 });
            tableSection.Add(new BuyGlCell("75000 GL", "За 30$", buy75000Gl) { Height = 300 });

            // Accomodate iPhone status bar.
            this.Padding = new Thickness(10, Device.OnPlatform(20, 0, 0), 10, 5);

            this.Content = new StackLayout
            {
                Children =
                {
                    tableView
                }
            };
        }

        public void buy5000Gl(object o, System.EventArgs e)
        {

        }

        public void buy75000Gl(object o, System.EventArgs e)
        {

        }
    }
}
