﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace GameLotto
{
    class BuyGlCell : ViewCell
    {
        public BuyGlCell(string number, string price, EventHandler onClick)
        {
            var titleLabel = new Label() { Text = number, FontSize = 26, TextColor = Color.FromRgb(43, 43, 43) };
            var priceLabel = new Label() { Text = price, FontSize = 17, TextColor = Color.FromRgb(166, 166, 166) };

            Grid grid = new Grid
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                RowDefinitions = {
                    new RowDefinition { Height = new GridLength(50, GridUnitType.Absolute) },
                    new RowDefinition { Height = new GridLength(40, GridUnitType.Absolute) },
                    new RowDefinition { Height = new GridLength(10, GridUnitType.Absolute) },
                },
                ColumnDefinitions = {
                    new ColumnDefinition { Width = new GridLength (150, GridUnitType.Absolute) },
                    new ColumnDefinition { Width = new GridLength (50, GridUnitType.Absolute) },
                    new ColumnDefinition { Width = new GridLength (100, GridUnitType.Absolute) }
                }
            };
            
            Button buyButton = new Button()
            {
                Text = "Купить",
                TextColor = Color.FromRgb(43, 43, 43),
                BackgroundColor = Color.FromRgb(255, 201, 0),
            };
            buyButton.Clicked += onClick;

            grid.Children.Add(titleLabel, 0, 0);
            grid.Children.Add(priceLabel, 0, 1);
            grid.Children.Add(buyButton, 2, 0);

            
            this.View = grid;
        }
    }
}
