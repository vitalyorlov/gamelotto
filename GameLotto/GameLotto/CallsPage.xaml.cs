﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace GameLotto
{
    public partial class CallsPage : ContentPage
    {
        public CallsPage()
        {
            InitializeComponent();
            this.Icon = "menu.png";
            NavigationPage.SetTitleIcon(this, this.Icon);
            NavigationPage.SetHasBackButton(this, true);
            this.BackgroundColor = Color.White;
        }
    }
}
