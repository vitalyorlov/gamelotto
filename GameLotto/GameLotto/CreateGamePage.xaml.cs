﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace GameLotto
{
    public partial class CreateGamePage : ContentPage
    {
        public CreateGamePage()
        {
            InitializeComponent();
            this.Icon = "menu.png";
            NavigationPage.SetTitleIcon(this, this.Icon);
            this.BackgroundColor = Color.FromRgb(236, 239, 245);

            StackLayout mainStack = new StackLayout()
            {
                BackgroundColor = Color.White
            };
           
            Grid tabs = new Grid
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                BackgroundColor = Color.FromRgb(41, 42, 56),
                RowDefinitions = {
                    new RowDefinition { Height = GridLength.Auto },
                    new RowDefinition { Height = GridLength.Auto },

                },
                ColumnDefinitions = {
                    new ColumnDefinition { Width = GridLength.Auto },
                    new ColumnDefinition { Width = GridLength.Auto },
                }
            };

            Button scoreButton = new Button()
            {
                Text = "На очки",
                BackgroundColor = Color.Transparent,
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };

            Button moneyButton = new Button()
            {
                Text = "На деньги",
                BackgroundColor = Color.Transparent,
                TextColor = Color.FromRgb(148, 149, 154),
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };

            tabs.Children.Add(scoreButton, 0, 0);
            tabs.Children.Add(moneyButton, 1, 0);

            BoxView box = new BoxView()
            {
                BackgroundColor = Color.White,
                HorizontalOptions = LayoutOptions.FillAndExpand
            };
            box.HeightRequest = 5;
            tabs.Children.Add(box, 0, 1);

            mainStack.Children.Add(tabs);
            
            StackLayout body = new StackLayout()
            {
                BackgroundColor = Color.White
            };
            body.Padding = new Thickness(10, 5, 10, 5);

            body.Children.Add(new Label()
            {
                Text = "Выберите количество игроков",
                TextColor = Color.Black
            });

            Grid playersTabs = new Grid
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                BackgroundColor = Color.Transparent,
                RowDefinitions = {
                    new RowDefinition { Height = GridLength.Auto },
                    new RowDefinition { Height = GridLength.Auto },
                },
                ColumnDefinitions = {
                    new ColumnDefinition { Width = GridLength.Auto },
                    new ColumnDefinition { Width = GridLength.Auto },
                    new ColumnDefinition { Width = GridLength.Auto },
                    new ColumnDefinition { Width = GridLength.Auto },
                }
            };

            Button oneOnOneButton = new Button()
            {
                Text = "1x1",
                BackgroundColor = Color.Transparent,
                TextColor = Color.Black,
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };

            Button sixButton = new Button()
            {
                Text = "6",
                BackgroundColor = Color.Transparent,
                TextColor = Color.Black,
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };

            Button twelveButton = new Button()
            {
                Text = "12",
                BackgroundColor = Color.Transparent,
                TextColor = Color.Black,
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };

            Button twentyFourButton = new Button()
            {
                Text = "24",
                BackgroundColor = Color.Transparent,
                TextColor = Color.Black,
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };

            playersTabs.Children.Add(oneOnOneButton, 0, 0);
            playersTabs.Children.Add(sixButton, 1, 0);
            playersTabs.Children.Add(twelveButton, 2, 0);
            playersTabs.Children.Add(twentyFourButton, 3, 0);

            BoxView playersBox = new BoxView()
            {
                BackgroundColor = Color.FromRgb(255, 86, 7),
                HorizontalOptions = LayoutOptions.FillAndExpand
            };
            playersBox.HeightRequest = 5;
            playersTabs.Children.Add(playersBox, 0, 1);

            body.Children.Add(playersTabs);

            body.Children.Add(new Label()
            {
                Text = "Выберите ставку, GL",
                TextColor = Color.Black
            });

            ///////////////////////////////
            Grid rateTabs = new Grid
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                BackgroundColor = Color.Transparent,
                RowDefinitions = {
                    new RowDefinition { Height = GridLength.Auto },
                    new RowDefinition { Height = GridLength.Auto },
                },
                ColumnDefinitions = {
                    new ColumnDefinition { Width = GridLength.Auto },
                    new ColumnDefinition { Width = GridLength.Auto },
                    new ColumnDefinition { Width = GridLength.Auto },
                    new ColumnDefinition { Width = GridLength.Auto },
                    new ColumnDefinition { Width = GridLength.Auto },
                }
            };

            Button tenButton = new Button()
            {
                Text = "10",
                FontSize = 13,
                BackgroundColor = Color.Transparent,
                TextColor = Color.Black,
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };

            Button twentyFiveButton = new Button()
            {
                Text = "25",
                FontSize = 13,
                BackgroundColor = Color.Transparent,
                TextColor = Color.Black,
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };

            Button hundredButton = new Button()
            {
                Text = "100",
                FontSize = 13,
                BackgroundColor = Color.Transparent,
                TextColor = Color.Black,
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };

            Button thousandButton = new Button()
            {
                Text = "1000",
                FontSize = 13,
                BackgroundColor = Color.Transparent,
                TextColor = Color.Black,
                HorizontalOptions = LayoutOptions.Start
            };

            Button tenThousandButton = new Button()
            {
                Text = "10000",
                FontSize = 13,
                BackgroundColor = Color.Transparent,
                TextColor = Color.Black,
                HorizontalOptions = LayoutOptions.Start
            };

            rateTabs.Children.Add(tenButton, 0, 0);
            rateTabs.Children.Add(twentyFiveButton, 1, 0);
            rateTabs.Children.Add(hundredButton, 2, 0);
            rateTabs.Children.Add(thousandButton, 3, 0);
            rateTabs.Children.Add(tenThousandButton, 4, 0);

            BoxView ratesBox = new BoxView()
            {
                BackgroundColor = Color.FromRgb(255, 86, 7),
                HorizontalOptions = LayoutOptions.FillAndExpand
            };
            ratesBox.HeightRequest = 5;
            rateTabs.Children.Add(ratesBox, 0, 1);

            body.Children.Add(rateTabs);
            
            mainStack.Children.Add(body);

            this.Content = mainStack;
            
        }
    }
}
