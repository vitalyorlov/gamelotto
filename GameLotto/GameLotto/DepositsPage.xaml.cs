﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace GameLotto
{
    public partial class DepositsPage : ContentPage
    {
        public DepositsPage()
        {
            InitializeComponent();
            this.Icon = "menu.png";
            NavigationPage.SetTitleIcon(this, this.Icon);
            this.BackgroundColor = Color.White;

            StackLayout mainStack = new StackLayout()
            {
                BackgroundColor = Color.White
            };

            Grid tabs = new Grid
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                BackgroundColor = Color.FromRgb(41, 42, 56),
                RowDefinitions = {
                    new RowDefinition { Height = GridLength.Auto },
                    new RowDefinition { Height = GridLength.Auto },

                },
                ColumnDefinitions = {
                    new ColumnDefinition { Width = GridLength.Auto },
                    new ColumnDefinition { Width = GridLength.Auto },
                }
            };

            Button activeGamesButton = new Button()
            {
                Text = "Депозит",
                BackgroundColor = Color.Transparent,
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };

            Button completedButton = new Button()
            {
                Text = "Вывод",
                BackgroundColor = Color.Transparent,
                TextColor = Color.FromRgb(148, 149, 154),
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };

            tabs.Children.Add(activeGamesButton, 0, 0);
            tabs.Children.Add(completedButton, 1, 0);

            BoxView box = new BoxView()
            {
                BackgroundColor = Color.FromRgb(255, 251, 0),
                HorizontalOptions = LayoutOptions.FillAndExpand
            };
            box.HeightRequest = 5;
            tabs.Children.Add(box, 0, 1);

            mainStack.Children.Add(tabs);

            this.Content = mainStack;

        }
    }
}
