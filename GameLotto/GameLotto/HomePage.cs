﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;

namespace GameLotto
{
    public class HomePage : MasterDetailPage
    {
        public HomePage()
        {

            /*var image = new Image { Aspect = Aspect.AspectFit };
            image.Source = ImageSource.FromFile("user.png");

            var testUser = new User//user created for testing, delete it in future
            {
                Login = "Heyman321",
                UserImage = image, 
                BalanceGL = 10000,
                BalanceMoney = 200
            };

            
            var loginInfo = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                Children =
                {
                    image,
                    new Label {
                    Text = testUser.Login,
                    VerticalOptions = LayoutOptions.Center
                    },
                }
            };

            var balanceInfo = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                Padding = 20,
                Children =
                {
                    new Label {
                    Text = testUser.BalanceGL.ToString(),
                    VerticalOptions = LayoutOptions.Center,
                    HorizontalOptions = LayoutOptions.Center
                    },
                    new Label {
                    Text = testUser.BalanceMoney.ToString(),
                    VerticalOptions = LayoutOptions.Center,
                    HorizontalOptions = LayoutOptions.Center
                    },
                }
            };
            */

            Label authLabel = new Label()
            {
                FontSize = 20,
                VerticalOptions = LayoutOptions.Center,
                TextColor = Color.White,
                Text = "Войти/Регистрация"
            };
            


            var nonAuthorizatedLayout = new StackLayout
            {
                Padding = 20,
                Children =
                {
                    authLabel
                }
            };

            var header = new StackLayout
            {
                Children =
                {
                    nonAuthorizatedLayout     
                }
            };

            NavigationPage mp = new NavigationPage(new MainPage()) {
                BarBackgroundColor = Color.FromRgb(51, 53, 70),
                BarTextColor = Color.White,
            };
            NavigationPage mgp = new NavigationPage(new MyGamesPage())
            {
                BarBackgroundColor = Color.FromRgb(51, 53, 70),
                BarTextColor = Color.White
            };
            NavigationPage dp = new NavigationPage(new DepositsPage())
            {
                BarBackgroundColor = Color.FromRgb(51, 53, 70),
                BarTextColor = Color.White
            };
            NavigationPage bglp = new NavigationPage(new BuyGLPage())
            {
                BarBackgroundColor = Color.FromRgb(51, 53, 70),
                BarTextColor = Color.White
            };
            NavigationPage cgp = new NavigationPage(new CreateGamePage())
            {
                BarBackgroundColor = Color.FromRgb(51, 53, 70),
                BarTextColor = Color.White
            };
            NavigationPage cp = new NavigationPage(new CallsPage())
            {
                BarBackgroundColor = Color.FromRgb(51, 53, 70),
                BarTextColor = Color.White
            };


            MenuItem[] menuItems = {
                new MenuItem("Главная", mp),
                new MenuItem("Мои игры", mgp),
                new MenuItem("Депозит/Вывод", dp),
                new MenuItem("Купить игровую валюту", bglp),
                new MenuItem("Создать Игру", cgp),
                new MenuItem("Вызовы", cp),
            };
           
            ListView listView = new ListView
            {
                ItemsSource = menuItems
            };

            listView.BackgroundColor = Color.FromRgb(41, 42, 56);
            listView.ItemTemplate = new DataTemplate(typeof(MenuItemCell));

            this.Master = new ContentPage
            {
                Title = "Menu",
                Content = new StackLayout
                {
                    Children = {
                        header,
                        listView
                    }
                },
                BackgroundColor = Color.FromRgb(41, 42, 56)

            };

            MainPage mainPage = new MainPage();
            this.Detail = mainPage;

            listView.ItemSelected += (sender, args) => {

                for (int i = 0; i < menuItems.Length; i++)
                {
                    if (menuItems[i].ToString() == args.SelectedItem.ToString())
                    {
                        this.Detail = menuItems[i].Page;
                        break;
                    }
                }
                
                this.IsPresented = false;
            };

            listView.SelectedItem = menuItems[0];
        }
    }
}
