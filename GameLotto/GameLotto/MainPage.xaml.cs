﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace GameLotto
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            this.Icon = "menu.png";
            NavigationPage.SetTitleIcon(this, this.Icon);
            this.BackgroundColor = Color.FromRgb(229, 226, 221);

        }
    }
}
