﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace GameLotto
{
    class MenuItem
    {
        public string Name { get; private set; }
        public NavigationPage Page { get; private set; }

        public MenuItem(string name, NavigationPage page)
        {
            this.Name = name;
            this.Page = page;
        }

        public override string ToString()
        {
            return Name;
        }

    }
}
