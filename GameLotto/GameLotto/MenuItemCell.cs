﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace GameLotto
{
    public class MenuItemCell : ViewCell
    {
        public MenuItemCell()
        {
            StackLayout cellWrapper = new StackLayout();
            StackLayout horizontalLayout = new StackLayout();
            BoxView box = new BoxView
            {
                Color = Color.Transparent,
                WidthRequest = 5,
            };
            Label left = new Label();
            
            left.SetBinding(Label.TextProperty, "Name");
            
            cellWrapper.BackgroundColor = Color.FromRgb(51, 53, 70);
            horizontalLayout.Orientation = StackOrientation.Horizontal;

            left.TextColor = Color.FromHex("#ffffff");
            left.VerticalOptions = LayoutOptions.CenterAndExpand;

            horizontalLayout.Children.Add(box);
            horizontalLayout.Children.Add(left);
            cellWrapper.Children.Add(horizontalLayout);
            View = cellWrapper;
        }
    }
}
