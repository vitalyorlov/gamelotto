﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace GameLotto
{
    public partial class MyGamesPage : ContentPage
    {
        public MyGamesPage()
        {
            InitializeComponent();
            this.Icon = "menu.png";
            NavigationPage.SetTitleIcon(this, this.Icon);
            this.BackgroundColor = Color.White;

            StackLayout mainStack = new StackLayout()
            {
                BackgroundColor = Color.White
            };

            Grid tabs = new Grid
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                BackgroundColor = Color.FromRgb(41, 42, 56),
                RowDefinitions = {
                    new RowDefinition { Height = GridLength.Auto },
                    new RowDefinition { Height = GridLength.Auto },

                },
                ColumnDefinitions = {
                    new ColumnDefinition { Width = GridLength.Auto },
                    new ColumnDefinition { Width = GridLength.Auto },
                }
            };

            Button activeGamesButton = new Button()
            {
                Text = "Активные игры",
                BackgroundColor = Color.Transparent,
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };

            Button completedButton = new Button()
            {
                Text = "Завершенные",
                BackgroundColor = Color.Transparent,
                TextColor = Color.FromRgb(148, 149, 154),
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };

            tabs.Children.Add(activeGamesButton, 0, 0);
            tabs.Children.Add(completedButton, 1, 0);
            
            BoxView box = new BoxView()
            {
                BackgroundColor = Color.FromRgb(255, 251, 0),
                HorizontalOptions = LayoutOptions.FillAndExpand
            };
            box.HeightRequest = 5;
            tabs.Children.Add(box, 0, 1);
            
            mainStack.Children.Add(tabs);

            Grid grid = new Grid
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
                RowDefinitions = {
                    new RowDefinition { Height = GridLength.Auto },
                },
                ColumnDefinitions = {
                    new ColumnDefinition { Width = GridLength.Auto },
                    new ColumnDefinition { Width = GridLength.Auto },
                    new ColumnDefinition { Width = GridLength.Auto }
                }
            };

            grid.Children.Add(new Label()
            {
                Text = "Выигрыш",
                TextColor = Color.Black,
                HorizontalOptions = LayoutOptions.CenterAndExpand
            }, 0, 0);
            grid.Children.Add(new Label()
            {
                Text = "Ставка",
                TextColor = Color.Black,
                HorizontalOptions = LayoutOptions.CenterAndExpand
            }, 1, 0);
            grid.Children.Add(new Label()
            {
                Text = "Место",
                TextColor = Color.Black,
                HorizontalOptions = LayoutOptions.CenterAndExpand
            }, 2, 0);
            grid.Children.Add(new Label()
            {
                Text = "Дата",
                TextColor = Color.Black,
                HorizontalOptions = LayoutOptions.CenterAndExpand
            }, 3, 0);


            mainStack.Children.Add(tabs);
            mainStack.Children.Add(grid);

            this.Content = mainStack;

        }
    }
}
