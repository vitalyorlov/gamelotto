﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace GameLotto
{
    class User
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public int BalanceGL { get; set; }
        public int BalanceMoney { get; set; }       
        public string Email { get; set; }
        public Image UserImage { get; set; }
        
    }
}
